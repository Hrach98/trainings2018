#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Please enter first number: ";
    std::cin >> number1;
    std::cout << "Please enter second number: ";
    std::cin >> number2;
    if (number1 > number2) {
        std::cout << number1  << " is larger." << std::endl;
    } 
    if (number2 > number1) {
        std::cout << number2 << " is larger." << std::endl;
    }
    if (number1 == number2) {
        std::cout << "These numbers are equal." << std::endl;
    }
    return 0;
}

