2.10 Indicate which of the following is true or false. Explain your answers.
a) Operations in C ++ are performed from left to right.
false: with the exception of the assignment operation

b) All of these variable names are valid: _under_bar_, m928134, t5, j7, her_sales, his_account_njnfk, a, b, c, z, z2.
true: The identifier may consist of letters, numbers and symbols
underscores (_), not starting with a digit.

c) operator cout<< "a = 5;" - typical example of an assignment operator.
false. this is an output statement

d) Correct C ++ arithmetic expression without parentheses
runs from left to right.
false. C ++ applies operations to operands of arithmetic expressions defined by rules
seniority (or priority) of operations, which practically coincide with those that
take place in algebra:

e) All of these variable names are invalid: 3g, 87, 67h2, h22, 2h.
false: h22 are valid, variable names are don't starting with a digit.
