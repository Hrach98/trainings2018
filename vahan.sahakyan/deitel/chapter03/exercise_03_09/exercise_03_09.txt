A program could use class string without inserting a using declaration, if we write "std" and the binary scope resolution operator (::) before each occurrence of "string".
Like this: std::string
