#include "Invoice.hpp"
#include <iostream>

Invoice::Invoice(std::string number, std::string description, int quantity, int price)
{
    setPartNumber(number);
    setPartDescription(description);
    setItemQuantity(quantity);
    setPricePerItem(price);
}

void
Invoice::setPartNumber(std::string number)
{
    partNumber_ = number;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

void
Invoice::setPartDescription(std::string description)
{
    partDescription_ = description;
}

std::string
Invoice::getPartDescription()
{
    return partDescription_;
}

void
Invoice::setItemQuantity(int quantity)
{
    if (quantity < 0) {
        std::cerr << "WARNING 1: Quantity cannot be negative. Quantity set to 0.\n";
        itemQuantity_ = 0;
        return;
    }
    itemQuantity_ = quantity;
}

int
Invoice::getItemQuantity()
{
    return itemQuantity_;
}

void
Invoice::setPricePerItem(int price)
{
    if (price < 0) {
        std::cerr << "WARNING 2: Price cannot be negative. Price set to 0.\n";
        pricePerItem_ = 0;
        return;
    }
    pricePerItem_ = price;
}

int
Invoice::getPricePerItem()
{
    return pricePerItem_;
}

int
Invoice::getInvoiceAmount()
{
    return getPricePerItem() * getItemQuantity();
}

