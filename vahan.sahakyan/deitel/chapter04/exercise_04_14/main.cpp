#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    while (true) {
/// Account Number
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter account number (-1 to end): ";
        }
        int accountNumber;
        std::cin >> accountNumber;
        if (-1 == accountNumber) { /// Sentinel Controlled Exit
            return 0;
        }
        if (accountNumber < 0) {
            std::cerr << "Error 1: Wrong account number!\n";
            return 1;
        }
/// Beginning Balance
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter beginning balance: ";
        }
        double beginningBalance;
        std::cin >> beginningBalance;
        if (beginningBalance < 0) {
            std::cerr << "Error 2: Wrong beginning balance!\n";
            return 2;
        }
/// Total Charges
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter total charges: ";
        }
        double totalCharges;
        std::cin >> totalCharges;
        if (totalCharges < 0) {
            std::cerr << "Error 3: Wrong total charges!\n";
            return 3;
        }
/// Total Credits
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter total credits: ";
        }
        double totalCredits;
        std::cin >> totalCredits;
        if (totalCredits < 0) {
            std::cerr << "Error 4: Wrong total credits!\n";
            return 4;
        }
/// Credit Limit
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter credit limit: ";
        }
        double creditLimit;
        std::cin >> creditLimit;
        if (creditLimit < 0) {
            std::cerr << "Error 5: Wrong credit limit!\n";
            return 5;
        }
/// New Balance
        double newBalance = beginningBalance + totalCharges - totalCredits;
/// Program Output
        std::cout << "New balance is " << newBalance << "\n" << std::endl;
        if (newBalance > creditLimit) {
            std::cout << "Account: " << std::setprecision(2) << std::fixed << accountNumber << std::endl;
            std::cout << "Credit limit: " << std::setprecision(2) << std::fixed << creditLimit << std::endl;
            std::cout << "Balance: " << newBalance << std::endl;
            std::cout << "Credit Limit Exceeded.\n" << std::endl;
        }
    }

    return 0;
}

