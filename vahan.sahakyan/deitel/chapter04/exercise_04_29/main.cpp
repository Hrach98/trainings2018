#include <iostream>

int
main()
{
    int number = 1;

    while (true) {
        number *= 2;
        std::cout << number << ", ";
        if (number == 0) {
            std::cerr << "\nERROR 1: Integer Overflow!\n";
            return 1;
        }
    }
    std::cout << std::endl;
    return 0;
}

