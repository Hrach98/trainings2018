/// Find the error(s) in each of the following:

///a.
For (x = 100, x >= 1, x++)
cout << x << endl;
/// answer: step statement is wrong (should be x--), braces missing, 'For' is entitled


///b. The following code should print whether integer v alue is odd or even:
switch (value % 2)
{
case 0:
    cout << "Even integer" << endl;
case 1:
    cout << "Odd integer" << endl;
}
/// answer: "break;" statements missing


/// c. The following code should output the odd integers from 19 to 1:
for ( x = 19; x >= 1; x += 2 )
    cout << x << endl;
/// answer: step expression is wrong (should be -=), and braces missing


/// d. The following code should output the even integers from 2 to 100:
counter = 2;
do
{
    cout << counter << endl;
    counter += 2;
} While (counter < 100);
/// answer: while statement entitled
