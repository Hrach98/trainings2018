#include <iostream>

int
main()
{
    int number1, number2;

    std::cout << "Enter first number: ";
    std::cin >> number1;
    
    std::cout << "Enter second number: ";
    std::cin >> number2;

    if (number1 > number2) {
        std::cout << number1 << " is larger." << std::endl;
        return 0;
    }

    if (number2 > number1) {
        std::cout << number2 << " is larger." << std::endl;
        return 0;
    }
    std::cout << "These numbers are equal." << std::endl;
    return 0;
}
