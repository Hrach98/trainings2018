#include <iostream>

int
main()
{
    int radius;
    std::cout << "Enter radius: ";
    std::cin >> radius;
    if (radius <= 0) {
        std::cout << "Error 1: The radius cannot be less than 0." << std::endl;
	return 1;
    }
    std::cout << "Diameter = " << 2 * radius << std::endl;
    std::cout << "Circumference = " << 2 * 3.14159 * radius << std::endl;
    std::cout << "Area = " << 2 * 3.14159 * radius * radius << std::endl;
    return 0;
}
