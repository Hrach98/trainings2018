#include <iostream>

int
main()
{
    std::cout << "* * * * * * * * " << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * * " << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * * " << std::endl;
    std::cout << " * * * * * * * *" << std::endl;
    std::cout << "* * * * * * * * " << std::endl;
    std::cout << " * * * * * * * *" << std::endl;

    std::cout << std::endl;

    std::cout << "* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * *\n";

    std::cout << std::endl;

    std::cout << "* * * * * * * * " << std::endl
	      << " * * * * * * * *" << std::endl
	      << "* * * * * * * * " << std::endl
	      << " * * * * * * * *" << std::endl
	      << "* * * * * * * * " << std::endl
	      << " * * * * * * * *" << std::endl
	      << "* * * * * * * * " << std::endl
	      << " * * * * * * * *" << std::endl;
    
    return 0;
}
