<?php
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
    echo '<html';
    echo    '<head>';
    echo        '<title>Portfolio</title>';
    echo    '</head>';
    echo    '<body>';
    echo        '<h1>Narek Zohrabyan</h1>';    
    $test = array(
        'About me' => '<h3>Skills</h3><p>
        <ul>    
            <li>Scripting languages (beginner level): TCL, SHELL(Bash)</li>
            <li>VIM</li>
            <li>GIT</li>
            <li>Programming languages (beginner level): C++, Python</li>
            <li>Web development tools (beginner level): PHP, JavaScript,  HTML / CSS, Joomla</li>
            <li>Operating Systems:  Linux(Ubuntu), Windows (10, 8.1(8), 7, XP) </li>
            <li>Languages: English, Russian</li>
        </ul>
                    
        <h3>Education</h3>
        <p>Degree:  Bachelor, in the field of  “Computer Engineering and Software Systems ”</p>
        <ul>
            <li>2012 September –  2016 May. The State Engineering University of Armenia <a href="http://www.seua.am" target="_blank">SEUA</a>, Yerevan, RA faculty of Computer Systems & Information, department of  Information Security Software</li>
            <li>2011 October - 2014 July. <a href="http://www.tumo.org" target="_blank">TUMO </a>Center  For  Creative Technologies, Yerevan, RA</li>
            <li>2007 – 2012 Yerevan base college of The State Engineering University of Armenia (SEUA), Yerevan, RA</p></li>
        </ul>',
        
        'Projects' => '<p><a href="https://gitlab.com/narek.zohrabyan/trainings2015/tree/master/narek.zohrabyan" target="_blank">HERE</a> you can find my projects!</p>',
        
        'CV' => '<p><a href="https://www.dropbox.com/s/npyyn27zjk3vceg/New%20.pdf?dl=0" target="_blank">Click here</a> to download CV file</p>',
        
        'Contacts' => '<p>Email: <a href="mailto:narek.zohrabyan95@gmail.com?subject=Message from site"> Write to narek.zohrabyan95@gmail.com</a> <br />Mobile: +37495 09 16 49</p>');
    
    foreach ($test as $key=> $value) {
        echo    "<div><h2>$key</h2>$value</div>";
    }
    echo        '</body>';
    echo '</html>';
?>