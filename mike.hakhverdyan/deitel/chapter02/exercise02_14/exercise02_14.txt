Exercise 2.14
Given the algebraic equation y = a*x^(3) + 7, which of the following, if any, are correct C++ statements for this equation?
a. y = a * x * x * x + 7 ;
b. y = a * x * x * ( x + 7 );
c. y = ( a * x ) * x * ( x + 7 );
d. y = (a * x) * x * x + 7 ;
e. y = a * ( x * x * x ) + 7 ;
f. y = a * x * ( x * x + 7 );

Answer:

a. True
b. False
c. False
d. True
e. True
f. False
