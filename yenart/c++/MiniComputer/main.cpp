#include "MiniComputer.hpp"

#include <iostream>

int
main()
{
    MiniComputer mini;
    mini.setComputerName("Mini");
    std::cout << mini.getComputerName() << std::endl;
    return 0;
}

