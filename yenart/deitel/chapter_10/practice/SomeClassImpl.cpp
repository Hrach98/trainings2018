#include "SomeClassImpl.hpp"

SomeClassImpl::SomeClassImpl(const int someVariable)
    : someVariable_(someVariable)
{
}

void
SomeClassImpl::someFunction()
{
    someAnotherFunction();
    someAnotherFunction();
    someAnotherFunction();
}

inline void
SomeClassImpl::someAnotherFunction()
{
    someVariable_ *= 9;
    someVariable_ %= 8;
}

