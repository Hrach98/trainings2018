#include "Employee.hpp"
#include <iostream>

int
main()
{
    Employee employee1("James", "Thomson", 300000);
    Employee employee2("Sally", "Shelton", 500000);
    Employee trainee("Mark", "Johnson", -100000);
    std::cout << "Mark's monthly salary is " << trainee.getSalary() << " USD\n";

    std::cout << "Yearly salary of " << employee1.getFirstName() << " " << employee1.getLastName() << " is " << employee1.getSalary() << " USD\n";
    std::cout << "Yearly salary of " << employee2.getFirstName() << " " << employee2.getLastName() << " is " << employee2.getSalary() << " USD\n";

    std::cout << "After 10% raise, the yearly salary of " << employee1.getFirstName() << " " << employee1.getLastName() << " is " << employee1.getSalary() * 1.1 << " USD\n";
    std::cout << "After 10% raise, the yearly salary of " << employee2.getFirstName() << " " << employee2.getLastName() << " is " << employee2.getSalary() * 1.1 << " USD\n";

    return 0;
}

