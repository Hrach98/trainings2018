#include "GradeBook.hpp"
#include <iostream>
#include <string>

int
main()
{
    GradeBook gradeBook1("CS101 Introduction to C++ Programming", "Suren Khachatryan");
    gradeBook1.displayMessage();
    gradeBook1.setInstructorName("Raymond Zavodnik");
    gradeBook1.setCourseName("CS 310 - Theory of Computing");
    gradeBook1.displayMessage();

    return 0;
}

