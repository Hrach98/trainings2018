State the order of evaluation of the operators in each of the following C++ statements and show the value of x after each statement is performed. 
1. x = 7 + 3 * 6 / 2 - 1;
	1) 3 * 6  2) 18 / 2  3) 7 + 9  4) 16 - 1  5) x = 15

2. x = 2 % 2 + 2 * 2 - 2 / 2;
	1) 2 % 2  2) 2 * 2  3) 2 / 2  4) 0 + 4  5) 4 - 1  6) x = 3
 
3. x = ( 3 * 9 * ( 3 + ( 9 * 3 / ( 3 ) ) ) );
	1) 9 * 3  2) 27 / 3  3) 3 + 9  4) 3 * 9  5) 27 * 12  6) x = 324 
	
