/// Program that asks user to insert two numbers and compares that numbers
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    int number1; /// first integer to compare
    int number2; /// second integer to compare

    std::cout << "Please insert the first number: "; /// prompt user to enter the first number
    std::cin >> number1;
    std::cout << "Please insert the second number: "; /// prompt user to enter the second number
    std::cin >> number2;

    if (number1 > number2) {
        std::cout << number1 << " is larger. " << std::endl;
        return 0;
    }
    if (number1 < number2) {
        std::cout << number2 << " is larger. " << std::endl;
        return 0;
    }

    std::cout << "These numbers are equal " << std::endl;

    return 0; /// indicates that program ended successfully
} /// end function main

