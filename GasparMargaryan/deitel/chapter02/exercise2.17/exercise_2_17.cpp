/// program that prints numbers 1 to 4 on the same line separated by a space in several ways
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// Using one statement with one stream insertion operator
    std::cout << "1 2 3 4" << std::endl;

    /// Using one statement with four stream insertion operators
    std::cout << "1 " << "2 " << "3 " << "4" << std::endl;

    /// Using four statements
    std::cout << "1 ";
    std::cout << "2 ";
    std::cout << "3 ";
    std::cout << "4" << std::endl;

    return 0; /// indicate that program ended successfully
} /// end function main

