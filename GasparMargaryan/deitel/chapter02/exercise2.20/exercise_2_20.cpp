/// program that reads in the radius of a circle as an integer
/// and prints the circle's diameter, circumference and area
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
   int pi = 3.14159;
   int radius;
   std::cout << "Please input a positive integer as a circles radius: "; /// prompt user to input circles radius
   std::cin >> radius; /// read the radius

   if (radius < 0) {
       std::cout << "Error 1: Radius can't be negative " << std::endl;
       return 1;
   }

   std::cout << "Circles diameter is " << (2 * radius) << std::endl;
   std::cout << "Circles circumference is " << (2 * pi * radius) << std::endl;
   std::cout << "Circles area is " << (pi * radius * radius) << std::endl;

   return 0; /// indicates that program ended successfully
} /// end of function main

