/// Program that reads in two integers and determines and prints
/// if the first is a multiple of the second
#include <iostream> /// allow program perform input and output

/// function main begins program execution
int
main()
{
    int number1, number2; /// declares two integers that user should insert
    std::cout << "Please enter two integers: "; /// prompt user to enter integers
    std::cin >> number1 >> number2;
    /// 0 devision error reduce
    if (0 == number1) {
        std::cout << "Error 1: the first integer should be positive. " << std::endl;
        return 1;
    }
    if (0 == number2 % number1) {
        std::cout << number1 << " is the multiple of " << number2 << std::endl;
        return 0;
    }
    std::cout << number1 << " is not the multiple of " << number2 << std::endl;

    return 0; /// indicate that program finished successfully
} /// end of function main

