The signed integer rage is from -2147483648 to 2147483647. After 13 factorial the printed value is incorrect because of integer overflow 
(the value of 13! is 1932053504, and the next value (i.e. 14!) is grater than 2147483647).
