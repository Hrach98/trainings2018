/// Exercise5_10
/// Program that prints the factorial from 1 to 5 in tabular format
#include <iostream>

int
main()
{
    std::cout << "N \t" << "Factorial of N " << std::endl;
    int factorial = 1;
    for (int counter = 1; counter <= 5; ++counter) {
        factorial *= counter;
        std::cout << counter << "\t" << factorial << " " << std::endl;
    }

    return 0; /// indicate successful termination
} /// end main

