/// Exercise 5_11
/// Modified program for compound interest calculations with for.

#include <iostream>
#include <iomanip>

int
main()
{
    /// set floating-point number format
    std::cout << std::fixed << std::setprecision(2);
    /// calculate amount on deposit for each of ten years
    double principal = 1000.0; /// initial amount before interest
    for (double rate = 5; rate <= 10; ++rate) {
        std::cout << "\nInterest rate " << rate << std::endl;
        /// display headers
        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
        double amount = principal; /// amount on deposit at end of each year
        for (int year = 1; year <= 10; ++year) { /// calculate new amount for specified year
            amount += amount * rate / 100;
            /// display the year and the amount
            std::cout << std::setw(4) << year << std::setw(21) << amount << std::endl;
        } /// end for
    }
    return 0; /// indicate successful termination
} // end main

