1.
For (x = 100, x >= 1, x++)
    cout << x << endl;
solution:
1) for (int x = 100; x >= 0; x--) {
       cout << x << endl;
   }

2) for (int x = 0; x <= 100; x++) {
       cout << x << endl;
   }

"For" must be "for", comas "," must be semicolons ";", "x" must be declared
"x" should decrement because it will be infifite loop, or "x" should be assigned to 0 and condition for "x" should be >= 100.

2.
switch (value % 2) {
    case 0:
        cout << "Even integer" << endl;
    case 1:
        cout << "Odd integer" << endl;
}

solution:
switch (value % 2) {
    case 0:
        cout << "Even integer" << endl;
        break;
    case 1:
        cout << "Odd integer" << endl;
        break;
    default: /// optional.
        break;
}

there must be break after each case

3.
for (x = 19; x >= 1; x += 2)
    cout << x << endl;

solution:

a) for (int x = 19; x >= 1; x -= 2)
        cout << x << endl;
"x" must be declared and decrement

b) for (int x = 1; x <= 19; x += 2)
        cout << x << endl;
"x" must be declared and assigned to 1, conndition - x >= 19

4.
counter = 2;
do
{
   cout << counter << endl;
   counter += 2;
} While (counter < 100);

solution:

int counter = 2;
do
{
   cout << counter << endl;
   counter += 2;
} while (counter <= 100);

counter must be declared, "While" must be "while", counter must be <= 100.

