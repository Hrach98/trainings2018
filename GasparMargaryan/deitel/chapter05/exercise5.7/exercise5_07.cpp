// Exercise 5.7: ex05_07.cpp
// What does this program print?
#include <iostream>
#include <unistd.h>
int
main()
{  int x; /// declare x
   int y; /// declare y
   if (::isatty(STDIN_FILENO)) { /// prompting user to input
       std::cout << "Enter two integers in the range 1-20: " << std::endl;
   }
   std::cin >> x >> y;  /// read values for x and y
   if (x < 1 || x > 20) {
       std::cout << "Error 1: Out of rage 1-20!!! " << std::endl;
       return 1;
   }
   if (y < 1 || y > 20) {
       std::cout << "Error 2: Out of rage 1-20!!! " << std::endl;
       return 2;
   }

   for (int i = 1; i <= y; i++) { /// count from 1 to y
       for (int j = 1; j <= x; j++) { /// count from 1 to x
           std::cout << '@'; /// output @
       }
       std::cout << std::endl; /// begin new line
   } /// end outer for

   return 0; /// indicate successful termination
} /// end main

