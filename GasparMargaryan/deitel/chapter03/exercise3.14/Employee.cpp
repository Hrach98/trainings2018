/// Exercise3.14: Employee.cpp
/// Employee member-function definitions. This file contains
/// implementations of the member functions prototyped in Employee.hpp.
#include "Employee.hpp" /// include definition of class Employee
#include <iostream>

/// constructor initializes employee's first and last names as type string and the salary as type int
Employee::Employee(std::string firstName, std::string lastName, int salary)
{
    setFirstName(firstName);
    setLastName(lastName);
    setSalary(salary);
} /// end of Employee constructor

/// Employee class member function implementations
void
Employee::setFirstName(std::string firstName)
{
    firstName_ = firstName;
}

std::string
Employee::getFirstName()
{
    return firstName_;
}

void
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
}

std::string
Employee::getLastName()
{
    return lastName_;
}

void
Employee::setSalary(int salary)
{
    /// in order if salary is negative the it should be set 0
    if (salary < 0) {
        salary = 0;
    }
    salary_ = salary;
}

int
Employee::getSalary()
{
    return salary_;
}
/// end of class function implementation

