/// Exercise3.11: main.cpp
/// Account class demonstration after separating
/// its interface from its implementation.
#include "Account.hpp" /// include definition of class Account
#include <iostream> /// include standard library input output stream

/// function main begins program execution
int
main()
{
    Account account1(20000);
    Account account2(500);

    int credit1, debit1;
    std::cout << "First Account \n";
    std::cout << "Please, add an ammount /credit/: \n";
    std::cin >> credit1;
    account1.credit(credit1);
    std::cout << "Please, input an ammount to withdraw /debit/: \n";
    std::cin >> debit1;
    account1.debit(debit1);
    std::cout << "Current account balance is: " << account1.getAccountBalance() << std::endl;

    int credit2, debit2;
    std::cout << "Second Account \n";
    std::cout << "Please, add an ammount /credit/: \n";
    std::cin >> credit2;
    account2.credit(credit2);
    std::cout << "Please, input an ammount to withdraw /debit/: \n";
    std::cin >> debit2;
    account2.debit(debit2);
    std::cout << "Current account balance is: " << account2.getAccountBalance() << std::endl;
    return 0; /// indicate that the program terminated successfully
} /// end of function main

