/// Exercise3.13: main.cpp
/// Invoice class demonstration after separating
/// its interface from its implementation.
#include "Invoice.hpp" /// include definition of class Invoice
#include <iostream> /// include standard library input output stream

/// function main begins program execution
int
main()
{
    Invoice invoice1("A200", "Mother Board", 100, 77);
    Invoice invoice2("Q504", "Keyboard", 78, 4);
/// I gave a little form of invoice to print on the screen
    std::cout << "INVOICE No__  \n";
    std::cout << "Item Number\t" << "Item Name\t" << "Quantity\t" << "Price\t" << "Amount \n";
    std::cout << invoice1.getPartNumber() << "\t\t" << invoice1.getPartDescription() << "\t" << invoice1.getItemQuantity() << "\t\t" << invoice1.getItemPrice() << "\t" << invoice1.getInvoiceAmount() << "\n";
    std::cout << invoice2.getPartNumber() << "\t\t" << invoice2.getPartDescription() << "\t" << invoice2.getItemQuantity() << "\t\t" << invoice2.getItemPrice() << "\t" << invoice2.getInvoiceAmount() << std::endl;
    std::cout << "Total amount: ---------------- " << invoice1.getInvoiceAmount() + invoice2.getInvoiceAmount() << " --------------- " << std::endl;

    return 0; /// function main terminated successfully
} /// end of function main

