/// Exercise3.13 Invoice.hpp
#include <string>
/// Invoice class definition
class Invoice
{
public:
    Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int itemPrice); /// constructor that initializes Invoice data members
/// class Invoice member functions prototypes
    void setPartNumber(std::string partNumber);
    std::string getPartNumber();

    void setPartDescription(std::string partDescription);
    std::string getPartDescription();

    void setItemQuantity(int itemQuantity);
    int getItemQuantity();

    void setItemPrice(int itemPrice);
    int getItemPrice();

    int getInvoiceAmount(); /// function that gets the invoice amount
///data member declaration
private:
    std::string partNumber_;
    std::string partDescription_;
    int itemQuantity_;
    int itemPrice_;
}; /// end class Invoice

