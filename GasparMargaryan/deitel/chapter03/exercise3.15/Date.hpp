/// Exercise3.15 Date.hpp
#include <string>
/// Invoice date definition
class Date
{
public:
    Date(int years, int months, int days); /// constructor that initializes Date data members
/// class Date member functions prototypes
    void setYear(int years);
    int getYear();

    void setMonth(int months);
    int getMonth();

    void setDay(int days);
    int getDay();

    void displayMessage();

///data member declaration
private:
    int years_;
    int months_;
    int days_;
}; /// end class Date

