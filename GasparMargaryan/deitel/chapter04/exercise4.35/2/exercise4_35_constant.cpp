/// Exercise 4.35
/// Program that input an nonnegative integer value and make some mathematical calculations
#include <iostream>   /// input output standard library header
#include <iomanip>   ///  include input output manipulator
#include <unistd.h>

int
main()
{
    std::cout << "2. A program that estimates the value of the mathematical constant <e> \n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "By using formula \"e = 1 + 1/1! + 1/2! + 1/3! + ...\"\n" <<
            "enter a positive number for accuracy of <e>: " << std::endl;
    }
    int eAccuracy;  /// declare control number for our formula to estimate how many times to make summary (loop)
    std::cin >> eAccuracy;
    if (eAccuracy < 0) { ///  control number should be higher than 0
        std::cout << "Error 1: Invalid number. The number should be positive! " << std::endl;
        return 1;
    }
    double eConst = 1;  ///  initialize the constant value of "e" of double type
    double eCounter = 1;  /// initialize counter for our formula's while loop
    double eFactorial = 1;  /// initialize factorial of "e" of double type
    while (eCounter < eAccuracy) {  ///  loop for our final result
        eFactorial *= eCounter;
        eConst += 1 / eFactorial;
        eCounter++;
    }
    std::cout << "The estimate value of constant = " << std::setprecision (8) << std::fixed << eConst << std::endl;

    return 0; /// indicate successful termination for wrong result
} /// end main

