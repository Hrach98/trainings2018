/// Exercise 4.35
/// Program that input an nonnegative integer value and make some mathematical calculations
#include <iostream>   /// input output standard library header
#include <iomanip>   ///  include input output manipulator
#include <unistd.h>

int
main()
{
    std::cout << "3. A program that computes the value of e^x = 1 + x/1! + x^2/2! + x^3/3! + ..." << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "\nEnter a positive number for accuaracy of <e^x>: " << std::endl;
    }
    double eAccuracy;  ///  declare control number for our formula to estimate how many times to make summary (loop)
    std::cin >> eAccuracy;
    if (eAccuracy < 0) { ///  print error when control number is less than 0
        std::cout << "Error 1: Invalid number. The number should be positive! " << std::endl;
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "\nPlease enter a positive number for <x> value: " << std::endl;
    }
    double valueX;   ///  declare value for "x"
    std::cin >> valueX;
    if (valueX < 0) { ///  print error message when the value of entered "x" is less than 0
        std::cout << "Error 2: Invalid number. The number should be positive! " << std::endl;
        return 2;
    }
    double ePowerX = 1;
    double eFactorial = 1;
    double eCounter = 1;
    double controlValueX = valueX;  ///  initialize control number for value x to use primary value in further
    while (eCounter < eAccuracy) {  /// loop for out final result
        eFactorial *= eCounter;
        ePowerX += valueX / eFactorial;
        eCounter++;
        valueX *= controlValueX;
    }
    std::cout << "The estimate value of constant =" << std::setprecision (8) << std::fixed << ePowerX << std::endl;
    
    return 0; /// indicate successful termination for wrong result
} /// end main

