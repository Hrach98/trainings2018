For 32 bits maximum representable value of unsigned integer is 2^32 − 1 = 4,294,967,295, and for signed integer -2,147,483,648
to 2,147,483,647. Before sending the review I've tested the program with unsigned integer value and it didn't gave negative value.
I've asked from uncle Google, and he told me that integer overflow is just like automobile machanical odometer when digits turn
into 000000 from the last 999999 meter where there is no higher digit to change to a 1 /like 1000000/.
Virally we know that after 999999 is 1000000, but for limited bits /or digital meters/ the computer will see only the last 000000.
After incremention the next value will be 1000001, but also the computer will see 000001. So for signed integers the rage of owerflow
will be from -2,147,483,648 to 2,147,483,647 and for unsigned integer from 0 to 4,294,967,295.
In other words I understand what is happening.
