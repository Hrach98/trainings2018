/// Exercise 4.33
/// Program that input 3 integer values and print
/// whether they could represent the sides of a right triangle
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter the size of leg 1: " << std::endl;
    }   /// prompt user to enter integers as sizes of triangle
    double leg1;
    std::cin >> leg1;
    if (leg1 <= 0) {
        std::cout << "Error 1: Invalid number, the side can't be negative! " << std::endl;
        /// if entered integer is negative stop the program and print error message
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter the size of leg 2: " << std::endl;
    }
    double leg2;
    std::cin >> leg2;
    if (leg2 <= 0) {
        std::cout << "Error 1: Invalid number, the side can't be negative! " << std::endl;
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter the size of leg3: " << std::endl;
    }
    double leg3;
    std::cin >> leg3;
    if (leg3 <= 0) {
        std::cout << "Error 1: Invalid number, the side can't be negative! " << std::endl;
        return 1;
    }
    /// to find out hypotenuse and other two sides, we suppose that hypotenuse is grater than each side 
    double hypotenuse = leg1;
    double sideA = leg1;
    double sideB = leg1;
    if (leg2 >= hypotenuse) {
        hypotenuse = leg2;
    } else {
        sideA = leg2;
    }
    if (leg3 >= hypotenuse) {
        hypotenuse = leg3;
        sideA = leg2;
    } else {
        sideB = leg3;
    }
    if (hypotenuse >= sideA + sideB) {
        std::cout << "Error 2: Invalid sides sizes. Hypotenuse is grater than or equal to the sum of legs: " << std::endl;
        return 2;
    }
    if (hypotenuse * hypotenuse == sideA * sideA + sideB * sideB) {
        std::cout << "The sides can repesent a right triangle: " << std::endl;
    } else {
        std::cout << "The sides can't represent a right triangle: " << std::endl;
    }
    return 0; /// indicate successful termination for wrong result
} /// end main

