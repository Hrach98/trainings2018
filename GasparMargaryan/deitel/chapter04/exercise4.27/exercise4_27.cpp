/// Exercise 4.27
/// Program that input binary integer and print decimal equivalent
#include <iostream>
#include <unistd.h>

int main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter binary number to print out decimal equivalent " << std::endl;
    }  /// prompt user to input value
    int binary;  /// declare binary variable
    std::cin >> binary;  /// input
    int decimal = 0;  /// initialize decimal number
    int posValue = 1;  /// initialize positional value
    while (binary != 0) {
        int digit = binary % 10;
        if (digit < 0) {
            std::cout << "Error 1: Binary number must contain 1s and 0s only! " << std::endl;
            return 1;
        }
        if (digit > 1) {
            std::cout << "Error 1: Binary number must contain 1s and 0s only! " << std::endl;
            return 1;
        }
        std::cout << digit << " * " << posValue;
        decimal += digit * posValue;
        posValue *= 2;
        binary /= 10;
        std::cout << (binary != 0 ? " + ": "");
    }
    std::cout << "\n" << "The decimal equivalent of binary number is \n" << decimal << std::endl;
    return 0; /// indicate successful termination
} /// end main

