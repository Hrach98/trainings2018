/// Exercise4_19
/// Program for determining the first and the second winners of salesmen who sold maximum number of units.
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the number of sold units for each salesman !" << std::endl;
        /// prompt user to input the number of sold units
    }
    int counter = 1;
    int maximum1 = -2147483648; /// initialize the largest number of sold unit to the minimal size of type integer
    int maximum2 = -2147483648; /// initialize the second largest number of sold unit to the minimal size of type integer
    while (counter <= 10) {
        int number;
        std::cin >> number;
        if (number > maximum1) {
            maximum2 = maximum1;
            maximum1 = number;
        } else if (maximum2 < number) {
            maximum2 = number;
        }
        ++counter;
    }
    std::cout << "The first winner is the salesman with " << maximum1 << " total units sold. " << std::endl;
    std::cout << "The second winner is the salesman with " << maximum2 << " total units sold. " << std::endl;
        /// prints the winner with largest the number of total sold units

    return 0; /// indicate successful termination
} /// end main

