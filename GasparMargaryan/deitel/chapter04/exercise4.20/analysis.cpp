/// Exercise4.20 Analysis
/// Member-function definitions for class Analysis that
/// analyzes examination results.
#include <iostream>
/// include definition of class Analysis from Analysis.h
#include "analysis.hpp"
#include <unistd.h>

/// process the examination results of 10 students
void
Analysis::processExamResults()
{
    /// initializing variables in declarations
    int passes = 0; /// number of passes
    int failures = 0; /// number of failures
    int studentCounter = 1; /// student counter
    /// prompt user for input and obtain value from user
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter result (1 = pass, 2 = fail): ";
    }
    while (studentCounter <= 10) { /// process 10 students using counter-controlled loop
        int result; /// one exam result (1 = pass, 2 = fail)
        std::cin >> result; /// input result
        if (1 == result) {  /// if result is 1,
            ++passes;
            ++studentCounter;
            /// increment counter;
        } else if (2 == result) {  /// else result is not 1, so
            ++failures;
            ++studentCounter;
        } else {
            std::cout << "Please enter valid result (1 or 2)!: " << std::endl;
        }
    } /// end while

    /// termination phase; display number of passes and failures
    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;
    ///determine whether more than eight students passed
    if (passes > 8) {
        std::cout << "Raise tuition " << std::endl;
    }
} /// end function processExamResults

