a)

if (x < 10)
    if (y > 10)
       std::cout << "*****" << std::endl;
    else
       std::cout << "#####" << std::endl;
std::cout << "$$$$$" << std::endl;

when x = 9, y = 11

output:

*****
$$$$$

when x = 11, y = 9

output:

$$$$$

b)

if (x < 10) {
    if (y > 10)
       std::cout << "*****" << std::endl;
}
else {
    std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
}

when x = 9, y = 11

output:

*****

when x = 11, y = 9

output:

#####
$$$$$

