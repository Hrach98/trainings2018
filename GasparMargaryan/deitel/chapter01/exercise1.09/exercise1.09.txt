a. OOP is newer than structure programming but they are connected, the internal structure of object is often built using structure programming.
b. OOD typical steps are taking objects from real life, abstracting and modeling it, using classes that are related, modify them and encapsulate the behaviors and attributes in objects.
c. There are different kinds of messages people send to each other, common ways of communication such as talking, body and gesture language, different things and gadgets (letters, books, notes, phones and so on).
d. We enjoy music and listen the latest news on car radio while driving, we remote it by giving orders by buttons (turn on/off, volume up/down, track change and so on) but we don't know how exactly it was built, how it works and what is hidden inside it.
