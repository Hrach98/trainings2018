for (i = 0; i < 10; ++i) {
    for (j = 0; j < i; ++j) {
        document.write("*");
    }
    document.writeln("");
}
document.writeln("");

for (i = 10; i > 0; --i) {
    for (j = 0; j < i; ++j) {
        document.write("*");
    }
    document.writeln("");
}
document.writeln("");

for (i = 0; i < 10; ++i) {
    for (j = 0; j < i; ++j) {
        document.write(" ");
    }
    for (k = i; k < 10; ++k) {
        document.write("*");
    }
    document.writeln("");
}
document.writeln("");

for (i = 10; i > 0; --i) {
    for (j = 0; j < i; ++j) {
        document.write(" ");
    }
    for (k = i; k < 10; ++k) {
        document.write("*");
    }
    document.writeln("");
}
document.writeln("");

for (i = 0; i < 10; ++i) {
    for (j = 0; j <= i; ++j) {
        document.write("*");
    }
    for (k = i; k < 10; ++k) {
        document.write(" ");
    }
    document.write(" ");
    
    for (j = i; j < 10; ++j) {
        document.write("*");
    }
    for (k = 10 - i; k < 10; ++k) {
        document.write(" ");
    }
    document.write(" ");

    for (j = 0; j < i; ++j) {
        document.write(" ");
    }
    for (k = i; k < 10; ++k) {
        document.write('*');
    }
    document.write(" ");

    for (j = 0; j < 10 - i; ++j) {
        document.write(' ');
    }
    for (k = 9; k < 10 + i; ++k) {
        document.write('*');
    }
    document.writeln("");
}
