<?php
    echo '<section>';
    echo     '<div id="pyramid" class="article">';
    if (!is_null($this->get('exercises')) 
            && !empty($this->get('exercises'))) {
        echo     '<ul>';
        foreach ($this->get('exercises') as $key => $value) {
            echo     '<li>';
            echo         '<a href="'.URL.'/?site='.$key.'">'.$value.'</a>';
            echo     '</li>';
        }
            echo '</ul>';
    } else {
        echo     '<div class="alert alert-info">';
        echo         'There are no exercises yet.';
        echo     '</div>';
    }
    echo     '</div>';
    echo '</section>';
?>
