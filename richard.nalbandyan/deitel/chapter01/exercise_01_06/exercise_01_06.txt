Questions:
Fill in the blanks in each of the following statements:
a. Which logical unit of the computer receives information from outside the computer for use by the computer?
b. The process of instructing the computer to solve specific problems is called ...
c. What type of computer language uses English-like abbreviations for machine-language instructions?
d. Which logical unit of the computer sends information that has already been processed by the computer to various devices so that the
information may be used outside the computer?
e. Which logical unit of the computer retains information?
f. Which logical unit of the computer performs calculations?
g. Which logical unit of the computer makes logical decisions?
h. The level of computer language most convenient to the programmer for writing programs quickly and easily is ...
i. The only language that a computer directly understands is called that computer's ...
j. Which logical unit of the computer coordinates the activities of all the other logical units?
Answers:
a.Input unit
b.Programming
c.Machine-independent
d.Output unit
e.Memory unit
f.ALU(Arythmetic Logic Unit)
g.ALU(Arythmetic Logic Unit)
h.High Level
i.machine language(binary)
j.CPU(Central Processing Unit)